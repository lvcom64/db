<data name="Employees" serviceNamespace="emp" transports="http https local">
    <description>Service for work with employees DB</description>
    <config enableOData="false" id="hr_db">
        <property name="carbon_datasource_name">hr_db</property>
    </config>
<!--=================================================================-->
    <operation name="getEmployees">
        <description>Get employee list</description>
        <call-query href="getEmployees"/>
    </operation>

    <query id="getEmployees" useConfig="hr_db">
        <!--language=SQL-->
        <sql>select e.id_ as emplyeeId,
                    e.first_name_ as familyName,
                    e.second_name_ as firstName,
                    e.family_name_ as secondName,
                    hp.name_ as position,
                    hu.name_ as unit,
                    e.date_hired_ as hired,
                    e.date_fired_ as fired,
                    e.salary_ salary
             from hr_employee e
                      join hr_unit hu on e.unit_id_ = hu.id_
                      join hr_position hp on e.position_id_ = hp.id_;
                      </sql>
        <result defaultNamespace="emp" element="employees" rowName="employee">
            <element column="emplyeeId"  name="employeeId" xsdType="integer"/>
            <element column="familyName" name="familyName" xsdType="string"/>
            <element column="firstName"  name="firstName"  xsdType="string"/>
            <element column="secondName" name="secondName" xsdType="string"/>
            <element column="position"   name="position"   xsdType="string"/>
            <element column="unit"       name="unit"       xsdType="string"/>
            <element column="hired"      name="hired"      xsdType="date"/>
            <element column="fired"      name="fired"      xsdType="date"/>
            <element column="salary"     name="salary"     xsdType="integer"/>
        </result>
    </query>

<!--=================================================================-->

    <operation name="getEmployeeById">
        <description>Get employee list</description>
        <call-query href="getEmployeeById">
            <with-param name="employeeId" query-param="employeeId"/>
        </call-query>
    </operation>

    <query id="getEmployeeById" useConfig="hr_db">
        <!--language=SQL-->
        <sql>select e.id_ as emplyeeId,
                    e.first_name_ as familyName,
                    e.second_name_ as firstName,
                    e.family_name_ as secondName,
                    hp.name_ as position,
                    hu.name_ as unit,
                    e.date_hired_ as hired,
                    e.date_fired_ as fired,
                    e.salary_ salary
             from hr_employee e
                      join hr_unit hu on e.unit_id_ = hu.id_
                      join hr_position hp on e.position_id_ = hp.id_
            where e.id_=?;
        </sql>
        <result defaultNamespace="emp" element="employee">
            <element column="employeeId"  name="employeeId" xsdType="integer"/>
            <element column="familyName" name="familyName" xsdType="string"/>
            <element column="firstName"  name="firstName"  xsdType="string"/>
            <element column="secondName" name="secondName" xsdType="string"/>
            <element column="position"   name="position"   xsdType="string"/>
            <element column="unit"       name="unit"       xsdType="string"/>
            <element column="hired"      name="hired"      xsdType="date"/>
            <element column="fired"      name="fired"      xsdType="date"/>
            <element column="salary"     name="salary"     xsdType="integer"/>
        </result>
        <param name="employeeId" ordinal="1" sqlType="INTEGER">   <description>ИД сотрудника</description></param>
    </query>

<!--=================================================================-->

    <operation name="getEmployeesByUnitId">
        <description>Get employee list</description>
        <call-query href="getEmployeesByUnitId">
            <with-param name="unitId" query-param="unitId"/>
        </call-query>
    </operation>

    <query id="getEmployeesByUnitId" useConfig="hr_db">
        <!--language=SQL-->
        <sql>select e.id_ as emplyeeId,
                    e.first_name_ as familyName,
                    e.second_name_ as firstName,
                    e.family_name_ as secondName,
                    hp.name_ as position,
                    hu.name_ as unit,
                    e.date_hired_ as hired,
                    e.date_fired_ as fired,
                    e.salary_ salary
             from hr_employee e
                      join hr_unit hu on e.unit_id_ = hu.id_
                      join hr_position hp on e.position_id_ = hp.id_
             where hu.id_=?;
        </sql>
        <result defaultNamespace="emp" element="employees" rowName ="employee">
            <element column="emplyeeId"  name="employeeId" xsdType="integer"/>
            <element column="familyName" name="familyName" xsdType="string"/>
            <element column="firstName"  name="firstName"  xsdType="string"/>
            <element column="secondName" name="secondName" xsdType="string"/>
            <element column="position"   name="position"   xsdType="string"/>
            <element column="unit"       name="unit"       xsdType="string"/>
            <element column="hired"      name="hired"      xsdType="date"/>
            <element column="fired"      name="fired"      xsdType="date"/>
            <element column="salary"     name="salary"     xsdType="integer"/>
        </result>
        <param name="unitId" ordinal="1" sqlType="INTEGER">   <description>ИД подразделения</description></param>
    </query>

<!--=================================================================-->

    <operation name="getManagers">
        <description>Get managers list</description>
        <call-query href="getManagers"/>
    </operation>

    <query id="getManagers" useConfig="hr_db">
        <!--language=SQL-->
        <sql>select e.id_ as emplyeeId,
                    e.first_name_ as familyName,
                    e.second_name_ as firstName,
                    e.family_name_ as secondName,
                    hp.name_ as position,
                    hu.name_ as unit
             from hr_employee e
                      join hr_unit hu on e.unit_id_ = hu.id_
                      join hr_position hp on e.position_id_ = hp.id_
             where e.id_=hu.head_;
        </sql>
        <result defaultNamespace="emp" element="managers" rowName ="manager">
            <element column="emplyeeId"  name="employeeId" xsdType="integer"/>
            <element column="familyName" name="familyName" xsdType="string"/>
            <element column="firstName"  name="firstName"  xsdType="string"/>
            <element column="secondName" name="secondName" xsdType="string"/>
            <element column="position"   name="position"   xsdType="string"/>
            <element column="unit"       name="unit"       xsdType="string"/>
        </result>
    </query>

<!--=================================================================-->

    <operation name="getManagerByEmployeeId">
        <description>Get managers list</description>
        <call-query href="getManagerByEmployeeId">
            <with-param name="employeeId" query-param="employeeId"/>
        </call-query>
    </operation>

    <query id="getManagerByEmployeeId" useConfig="hr_db">
        <!--language=SQL-->
        <sql>select hu.head_ as headId
             from hr_unit hu
                      join hr_employee e on e.unit_id_ = hu.id_
             where e.id_=?;
        </sql>
        <result defaultNamespace="emp" element="manager">
            <element column="headId"  name="headId" xsdType="integer"/>
        </result>
        <param name="employeeId" ordinal="1" sqlType="INTEGER">   <description>ИД сотрудника</description></param>
    </query>

<!--=================================================================-->

    <operation name="addEmployee">
        <description>Add new employee</description>
        <call-query href="addEmployee">
            <with-param name="familyName" query-param="familyName"/>
            <with-param name="firstName"  query-param="firstName"/>
            <with-param name="secondName" query-param="secondName"/>
            <with-param name="unitId"     query-param="unitId"/>
            <with-param name="positionId" query-param="positionId"/>
            <with-param name="salary"     query-param="salary"/>
            <with-param name="dateHired"  query-param="dateHired"/>
        </call-query>
    </operation>

    <query id="addEmployee" useConfig="hr_db">
        <!--language=SQL-->
        <sql>
                BEGIN;
                    INSERT INTO hr_employee (first_name_,
                                             second_name_,
                                             family_name_,
                                             unit_id_,
                                             position_id_,
                                             salary_,
                                             date_hired_)
                    VALUES (?,?,?,?,?,?,?);
                    SELECT currval('hr_employee_id__seq');
                END;
        </sql>
        <result defaultNamespace="emp" element="employee" rowName="">
            <element column="currval" name="employeeId" namespace="emp" xsdType="integer"/>
        </result>
        <param name="familyName" ordinal="1" sqlType="STRING">   <description>Фамилия</description></param>
        <param name="firstName"  ordinal="2" sqlType="STRING">   <description>Имя</description></param>
        <param name="secondName" ordinal="3" sqlType="STRING">   <description>Отчество</description></param>
        <param name="unitId"     ordinal="4" sqlType="INTEGER">  <description>Код подразделения</description></param>
        <param name="positionId" ordinal="5" sqlType="INTEGER">  <description>Код позиции</description></param>
        <param name="salary"     ordinal="6" sqlType="INTEGER">  <description>Оклад</description></param>
        <param name="dateHired"  ordinal="7" sqlType="TIMESTAMP"><description>Дата приема</description></param>
    </query>

<!--=================================================================-->

    <operation name="fireEmployee">
        <description>Fire employee</description>
        <call-query href="fireEmployee">
            <with-param name="employeeId" query-param="employeeId"/>
            <with-param name="dateFired"  query-param="dateFired"/>
        </call-query>
    </operation>

    <query id="fireEmployee" useConfig="hr_db">
        <!--language=SQL-->
        <sql>
            UPDATE hr_employee
            SET date_fired_ = ?
            WHERE id_ = ?
            RETURNING id_, first_name_,second_name_, family_name_, date_fired_;
        </sql>
        <result defaultNamespace="emp"  element="employee" rowName="">
            <element column="id_"           name="employeeId" xsdType="integer"/>
            <element column="first_name_"   name="familyName" xsdType="string"/>
            <element column="second_name_"  name="firstName"  xsdType="string"/>
            <element column="family_name_"  name="secondName" xsdType="string"/>
            <element column="fired"         name="fired"      xsdType="date"/>
        </result>
        <param name="employeeId" ordinal="1" sqlType="INTEGER">   <description>Фамилия</description></param>
        <param name="dateFired"  ordinal="2" sqlType="TIMESTAMP"><description>Дата увольнения</description></param>
    </query>

<!--=================================================================-->

    <operation name="getManagerByUnitId">
        <description>Get managers list</description>
        <call-query href="getManagerByUnitId">
            <with-param name="unitId" query-param="unitId"/>
        </call-query>

    </operation>

    <query id="getManagerByUnitId" useConfig="hr_db">
        <!--language=SQL-->
        <sql>
            SELECT hu.head_ AS headId
            FROM hr_unit hu
            WHERE hu.id_ = ?;
        </sql>
        <result defaultNamespace="emp" element="manager">
            <element column="headId"  name="headId" xsdType="integer"/>
        </result>
        <param name="unitId" ordinal="1" sqlType="INTEGER">   <description>ИД подразделения</description></param>
    </query>

</data>
