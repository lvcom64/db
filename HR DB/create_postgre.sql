create table HR_EMPLOYEE (
                             ID_ serial,
                             FIRST_NAME_ varchar(50) not null,
                             SECOND_NAME_ varchar(50),
                             FAMILY_NAME_ varchar(100) not null,
                             UNIT_ID_ integer not null,
                             POSITION_ID_ integer not null,
                             SALARY_ integer not null,
                             DATE_HIRED_ timestamp not null,
                             DATE_FIRED_ timestamp,
                             primary key (ID_)
);

create table HR_UNIT (
                         ID_ serial,
                         NAME_ varchar(400) not null,
                         HEAD_ integer,
                         primary key (ID_)
);

create table HR_POSITION (
                             ID_ serial,
                             NAME_ varchar(400) not null,
                             primary key (ID_)
);

create table HR_VACATION (
                             ID_ serial,
                             EMPLOYEE_ID_ integer,
                             DATE_START_ timestamp not null,
                             DATE_END_ timestamp not null,
                             TYPE_ integer,
                             primary key (ID_)
);

create table HR_VACATION_TYPE (
                                  ID_ serial,
                                  NAME_ varchar(50) not null,
                                  primary key (ID_)
);

create index HR_IDX_EMPLOYEE_UNIT on HR_EMPLOYEE(UNIT_ID_);
alter table HR_EMPLOYEE
    add constraint HR_FK_EMPLOYEE_UNIT
        foreign key (UNIT_ID_)
            references HR_UNIT (ID_);
alter table HR_EMPLOYEE
    add constraint HR_FK_EMPLOYEE_POSITION
        foreign key (POSITION_ID_)
            references HR_POSITION (ID_);

create index HR_IDX_UNIT_HEAD on HR_UNIT(HEAD_);
alter table HR_UNIT
    add constraint HR_FK_UNIT_HEAD
        foreign key (HEAD_)
            references HR_EMPLOYEE (ID_);

create index HR_IDX_VACATION_START on HR_VACATION(DATE_START_);
create index HR_IDX_VACATION_END on HR_VACATION(DATE_END_);
create index HR_IDX_VACATION_EMPLOYEE on HR_VACATION(EMPLOYEE_ID_);
alter table HR_VACATION
    add constraint HR_FK_VACATION_EMPLOYEE
        foreign key (EMPLOYEE_ID_)
            references HR_EMPLOYEE (ID_);
alter table HR_VACATION
    add constraint HR_FK_VACATION_TYPE
        foreign key (TYPE_)
            references HR_VACATION_TYPE (ID_);


create function getvacationsbyemployeeidlastyear(id integer) returns integer
    language plpgsql
as
$$
DECLARE
    days1 int;
    days2 int;
    result int;
BEGIN
    result = 28;
    SELECT SUM((date_end_::date - date_start_::date)::INT) into days1
    FROM hr_vacation vacation
    WHERE employee_id_= id
      AND vacation.date_end_ > (CURRENT_TIMESTAMP - INTERVAL '1 year')
      AND vacation.date_start_ > (CURRENT_TIMESTAMP - INTERVAL '1 year');

    SELECT SUM((date_end_::date - (CURRENT_TIMESTAMP - INTERVAL '1 year')::date)::INT) into days2
    FROM hr_vacation vacation
    WHERE employee_id_= id
      AND vacation.date_end_ > (CURRENT_TIMESTAMP - INTERVAL '1 year')
      AND vacation.date_start_ <= (CURRENT_TIMESTAMP - INTERVAL '1 year');

    RETURN result - COALESCE (days1,0) - COALESCE (days2, 0);
END;
$$;

alter function getvacationsbyemployeeidlastyear(integer) owner to postgres;



create function getcrossingvacationsbyemployeeid(date1 timestamp without time zone, date2 timestamp without time zone, unitid integer) returns integer
    language plpgsql
as
$$
declare
    cross_count integer;
begin
    SELECT
        COUNT(hv.id_) into cross_count
    FROM
        hr_vacation hv
            JOIN hr_employee he ON he.id_ = hv.employee_id_
            JOIN hr_unit hu ON hu.id_ = he.unit_id_
    WHERE
        (
                (
                        (
                            hv.date_start_ between date1 and date2)
                        OR
                        (hv.date_end_ between date1 and date2)
                    )
                OR  (
                        (date1 between date_start_ and date_end_)
                        OR
                        (date2 between date_start_ and date_end_)
                    )
            )
      AND (hv.date_start_ > CURRENT_TIMESTAMP or hv.date_end_ > CURRENT_TIMESTAMP)
      AND he.unit_id_=unitid;
    return cross_count;
end;
$$;

alter function getcrossingvacationsbyemployeeid(timestamp, timestamp, integer) owner to postgres;

create function get_crossing_vacations_by_unit_id(date1 timestamp without time zone, date2 timestamp without time zone, employeeid integer) returns integer
    language plpgsql
as
$$
declare
    unitId integer;
    cross_count integer;
begin
    SELECT he.unit_id_ into unitId
    from hr_employee he
    where id_=employeeid;

    SELECT
        COUNT(hv.id_) into cross_count
    FROM
        hr_vacation hv
            JOIN hr_employee he ON he.id_ = hv.employee_id_
            JOIN hr_unit hu ON hu.id_ = he.unit_id_
    WHERE
        (
                (
                        (
                            hv.date_start_ between date1 and date2)
                        OR
                        (hv.date_end_ between date1 and date2)
                    )
                OR  (
                        (date1 between date_start_ and date_end_)
                        OR
                        (date2 between date_start_ and date_end_)
                    )
            )
      AND (hv.date_start_ > CURRENT_TIMESTAMP or hv.date_end_ > CURRENT_TIMESTAMP)
      AND he.unit_id_=unitid;
    return cross_count;
end;
$$;

alter function get_crossing_vacations_by_unit_id(timestamp, timestamp, integer) owner to postgres;

